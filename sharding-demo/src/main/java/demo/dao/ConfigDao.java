package com.example.demo.dao;

import com.example.demo.entity.TConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfigDao extends JpaRepository<TConfig, Integer> {
}

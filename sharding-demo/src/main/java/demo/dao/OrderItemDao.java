package com.example.demo.dao;

import com.example.demo.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface OrderItemDao extends JpaRepository<OrderItem, Long> {
    @Query(value = "select n from Order t inner join OrderItem n on t.orderId = n.orderId where n.orderId=:orderId")
    Optional<OrderItem> getOrderItemByOrderId(@Param("orderId") Long orderId);
}
